The main app file is main.ts - is used as an entry point of Webpack.
`main.ts` usually include main blocks:

Import js Libs with legacy components and plain js code.
`import './some.js';`

Style import.
`import './sass/main.scss';`

Import Vue and all plugins. We use i18n and route plugins
```
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueI18n from 'vue-i18n';
```

Setup app routs.
```
let router = new VueRouter({
  routes: [
    { path: '/', component: Page },
  ]
});
```

Import language file.
`import nl from './vue-components/locales/nl';`

Setup i18n with actual locale.
```
const i18n = new VueI18n({
  locale: ('def_locale'
  messages: {
    'nl': nl,
  }
})
```

Include plugins.
`Vue.use(VueRouter);`

Custom and contrib components import. Use {} to import class by name not by default.
`import { AppFooter }   from './components/layouts/app-footer';`

Create new Vue instance. Import translates and routes. Components declaration with HTML tegs.
```
new Vue({
  i18n,
  el: '#app-main',
  router: router,
  components: {
      'component-teg': Component,
  }
});
```

------
[https://vuejs.org/v2/guide/typescript.html](https://vuejs.org/v2/guide/typescript.html)
[https://herringtondarkholme.github.io/2016/10/03/vue2-ts2/](https://herringtondarkholme.github.io/2016/10/03/vue2-ts2/)
[https://alligator.io/vuejs/typescript-class-components/](https://alligator.io/vuejs/typescript-class-components/)
[https://router.vuejs.org/en/](https://router.vuejs.org/en/)
[http://kazupon.github.io/vue-i18n/en/](http://kazupon.github.io/vue-i18n/en/)

