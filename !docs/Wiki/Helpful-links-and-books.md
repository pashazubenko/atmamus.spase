Build and Test Run:
[https://github.com/s-panferov/awesome-typescript-loader](https://github.com/s-panferov/awesome-typescript-loader)
[http://www.jbrantly.com/typescript-and-webpack/](http://www.jbrantly.com/typescript-and-webpack/)
[https://habrahabr.ru/post/309306/](https://habrahabr.ru/post/309306/)

Vue setup
[https://vuejs.org/v2/guide/typescript.html](https://vuejs.org/v2/guide/typescript.html)
[https://herringtondarkholme.github.io/2016/10/03/vue2-ts2/](https://herringtondarkholme.github.io/2016/10/03/vue2-ts2/)
[https://alligator.io/vuejs/typescript-class-components/](https://alligator.io/vuejs/typescript-class-components/)
[https://router.vuejs.org/en/](https://router.vuejs.org/en/)
[http://kazupon.github.io/vue-i18n/en/](http://kazupon.github.io/vue-i18n/en/)

Components:
[https://vuejs.org/v2/style-guide/](https://vuejs.org/v2/style-guide/)
[https://github.com/vuejs/vue-class-component](https://github.com/vuejs/vue-class-component)
[https://github.com/kaorun343/vue-property-decorator](https://github.com/kaorun343/vue-property-decorator)
[https://www.typescriptlang.org/docs/handbook/namespaces.html](https://www.typescriptlang.org/docs/handbook/namespaces.html)

Providers & IoC Container.md:
[http://inversify.io](http://inversify.io)
[https://github.com/inversify/InversifyJS/blob/master/wiki/container_api.md](https://github.com/inversify/InversifyJS/blob/master/wiki/container_api.md)
[https://github.com/inversify/inversify-basic-example](https://github.com/inversify/inversify-basic-example)
[https://medium.com/@samueleresca/inversion-of-control-and-dependency-injection-in-typescript-3040d568aabe](https://medium.com/@samueleresca/inversion-of-control-and-dependency-injection-in-typescript-3040d568aabe)
[https://blog.kloud.com.au/2017/03/22/dependency-injection-in-vuejs-app-with-typescript/](https://blog.kloud.com.au/2017/03/22/dependency-injection-in-vuejs-app-with-typescript/)
[http://artemdemo.me/blog/inversifyjs-инверсия-управления-в-js-на-базе-typescriptа/](http://artemdemo.me/blog/inversifyjs-инверсия-управления-в-js-на-базе-typescriptа/)

Tests
[https://medium.com/powtoon-engineering/a-complete-guide-to-testing-javascript-in-2017-a217b4cd5a2a](https://medium.com/powtoon-engineering/a-complete-guide-to-testing-javascript-in-2017-a217b4cd5a2a)
[https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6](https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6)
[https://alligator.io/vuejs/unit-testing-karma-mocha/](https://alligator.io/vuejs/unit-testing-karma-mocha/)
[https://scotch.io/tutorials/how-to-write-a-unit-test-for-vuejs](https://scotch.io/tutorials/how-to-write-a-unit-test-for-vuejs)

Book:
[TypeScript Design Patterns](https://code.bajabikes.eu/docs/developer-start/wikis/New-Frontend/TypeScript-Design-Patterns.zip)
