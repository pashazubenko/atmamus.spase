import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({

    // Inner Data Structure
    state: {

    },

    // Outside communication with data.
    actions: {

    },

    // Logic that change data
    mutations: {

    },

    // Get Data
    getters: {

    },

    // Other Vuex Stores for separate structure.
    modules: {
      // a: moduleA, | const moduleA = { state: {}, mutations: {}, actions: {}, getters: {} }
    }
});

export default store;
