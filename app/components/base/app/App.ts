import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
  template: require('./app.html'),
})
class App extends Vue {

  // Lifecycle hook
  mounted () {

  }
}

export default App;
