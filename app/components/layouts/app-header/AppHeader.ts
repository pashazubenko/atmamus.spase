import Vue from 'vue';
import Component from 'vue-class-component';

import './app-header.scss';

@Component({
    template: require('./app-header.html'),
    props: {

    },
})
export class AppHeader extends Vue {

}
