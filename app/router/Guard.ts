
export default {

    guest (to, from, next) {
        next();
    },

    auth (to, from, next) {
        next(from);
    }
};

