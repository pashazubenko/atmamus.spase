For application tests, we use a bunch of plugins - Karma Mocha Chai and Sinon.
Karma is a test-runner with runs local tests based on headless Chrome or remote - use testbot launcher.
Mocha is test framework for asynchronous testing.
Chai is assertion library. Sinon provides test spies, stubs and mocks

Providers and components are unit tested as a rest of js tests.
For testing Vue Components u need mockup all dependencies, include template file (same as component).
This prepared `MockedComponent` should be put into `ComponentTest`. `ComponentTest` - wrapper, create empty
Vue instance and include the component with options.

Component wrapper allows as execute Vue instance for test assertions.

test.ts - Test runner file. It includes all `*.spec.ts` files in a project and run them.

TestBoot

-----
[https://medium.com/powtoon-engineering/a-complete-guide-to-testing-javascript-in-2017-a217b4cd5a2a](https://medium.com/powtoon-engineering/a-complete-guide-to-testing-javascript-in-2017-a217b4cd5a2a)
[https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6](https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6)
[https://alligator.io/vuejs/unit-testing-karma-mocha/](https://alligator.io/vuejs/unit-testing-karma-mocha/)
[https://scotch.io/tutorials/how-to-write-a-unit-test-for-vuejs](https://scotch.io/tutorials/how-to-write-a-unit-test-for-vuejs)
