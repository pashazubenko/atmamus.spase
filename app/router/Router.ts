import Vue from 'vue';
import Router from 'vue-router';

import Guard from './Guard';

Vue.use(Router);

// Page components
import { HomePage }  from '../components/pages/home-page';
import { PageNotFound }  from '../components/pages/page-not-found';


const router = new Router({
  mode: 'history',
  base: process.env.PATH_BASE,
  routes: [
    {
        path: '/',
        name: 'home-page',
        component: HomePage,
        beforeEnter: Guard.guest
    },
    // 404 - should be last
    { path: '*', name: '404', component: PageNotFound }
  ]});

export default router;
