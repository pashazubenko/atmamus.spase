import Vue from 'vue';
import Component from 'vue-class-component';

import { AppHeader } from '../../layouts/app-header';
import { AppFooter } from '../../layouts/app-footer';


import './home-page.scss';

@Component({
  template: require('./home-page.html'),
  components: {
    'app-header': AppHeader,
    'app-footer': AppFooter
  }
})
export class HomePage extends Vue {

}
