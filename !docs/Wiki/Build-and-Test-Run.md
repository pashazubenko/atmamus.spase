To build the project we use the npm scripts. Before build install all dependencies `npm install`.
Main Commands:
`build:prod` - Build Production version of App with mins end env variables.
`build:dev` - Build the develop version of App with maps end env variables.
`build:watch` - Develop watch function.
`clean` - Delete public folder.
`compile:prod` - Run Webpack with settings.
`compile:dev` - Run Webpack with settings.
`lint` - Check `*.ts` files for code style.
`test` - Run local tests with Karma and Headless Chrome.
`testingbot` - Run tests with Karma on the remote testingbot server.

To build the project we use Webpack.
All Webpack settings are split into several files:
`webpack.config.base.js` - Base Webpack configuration set input and output files.
`webpack.config.test.js` - Test Webpack env which build without assets.
`webpack.config.dev.js` - Config for Dev build include maps.
`webpack.config.prod.js` - Clear minimize build.

For most js code in the project used typescript.
To configure its compilation look to the file - `tsconfig.json`.
`tslint.json` - Linter rules configuration file.

The built code species to the environment and settings are placed in the folder.
Main App entry point created from template `src/app/index.html` and include final build assets.

-----
[https://github.com/s-panferov/awesome-typescript-loader](https://github.com/s-panferov/awesome-typescript-loader)
[http://www.jbrantly.com/typescript-and-webpack/](http://www.jbrantly.com/typescript-and-webpack/)
[https://habrahabr.ru/post/309306/](https://habrahabr.ru/post/309306/)
