For component organization at first read official Vue js code style guide.

Since we are using TypeScript, the basis for the component is the Class
extends Vue and use `vue-class-component` with the main set of TypeScript decorators `@Component`.
For a complete support of Vue use the `value-property-decorator` library.

Naming files:
`index.ts` - file for quick asses to component just by folder name
`component-name.html` - HTML template. Use kebab-case same as HTML tag name.
`component-name.scss` - Use separate files for component styles.
`ComponentName.spec.ts` - Test Class. Should be in the same folder as the component for closer asses to template and styles. Use upper camel case for name convention same as the class.
`ComponentName.ts` - Main Class file. One file for one component class.

Child components that are tightly coupled to their parent should include the parent component name as a prefix.
```
components/
|- TodoList/
|- TodoListItem/
|- TodoListItemButton/
```

With multiple nesting of components and a large number of dependencies, it is also likely to use the namespace.

------
[https://vuejs.org/v2/style-guide/](https://vuejs.org/v2/style-guide/)
[https://github.com/vuejs/vue-class-component](https://github.com/vuejs/vue-class-component)
[https://github.com/kaorun343/vue-property-decorator](https://github.com/kaorun343/vue-property-decorator)
[https://www.typescriptlang.org/docs/handbook/namespaces.html](https://www.typescriptlang.org/docs/handbook/namespaces.html)
