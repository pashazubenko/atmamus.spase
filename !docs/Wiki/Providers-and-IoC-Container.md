To increase the testability of the application, all dependent
parts providing any kind of fusion must be placed in the Provider classes
and delivered through the IoC Container  - `IocContainer.ts`.

The container stores a list of available interfaces and implementations list for it.
You need to give the Container the necessary interface -
the container will return the wanted interface implementation for you.

`let Provider = Container.get<Interface>(SERVICES.PROVIDER);`

`SERVICES.TYPE` - object with names of types
In large applications using strings as the identifiers of the types
to be injected by the Container can lead to naming collisions.
We use Symbols instead of string literals. A symbol is a unique and immutable data type
and used as an identifier for a provider.

Register the provider class and interface if necessary,
change the rules according to Container API

`myContainer.bind<Warrior>(TYPES.Warrior).to(Ninja);`

------
[http://inversify.io](http://inversify.io)
[https://github.com/inversify/InversifyJS/blob/master/wiki/container_api.md](https://github.com/inversify/InversifyJS/blob/master/wiki/container_api.md)
[https://github.com/inversify/inversify-basic-example](https://github.com/inversify/inversify-basic-example)
[https://medium.com/@samueleresca/inversion-of-control-and-dependency-injection-in-typescript-3040d568aabe](https://medium.com/@samueleresca/inversion-of-control-and-dependency-injection-in-typescript-3040d568aabe)
[https://blog.kloud.com.au/2017/03/22/dependency-injection-in-vuejs-app-with-typescript/](https://blog.kloud.com.au/2017/03/22/dependency-injection-in-vuejs-app-with-typescript/)
[http://artemdemo.me/blog/inversifyjs-инверсия-управления-в-js-на-базе-typescriptа/](http://artemdemo.me/blog/inversifyjs-инверсия-управления-в-js-на-базе-typescriptа/)
