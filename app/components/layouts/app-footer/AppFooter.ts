import Vue from 'vue';
import Component from 'vue-class-component';

import './app-footer.scss';

@Component({
    template: require('./app-footer.html')
})
export class AppFooter extends Vue {

}
